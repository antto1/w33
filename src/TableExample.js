import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import birds from './bird.json'
import { Typography } from '@mui/material';
const TableExample =()=>{

    console.log(JSON.stringify(birds));

    function compare( a, b ) {
        if ( a.finnish < b.finnish) {
            return -1;
        }
        if ( a.finnish > b.finnish){
            return 1;
        }
        return 0;
    }

    birds.sort(compare);

    function createData(finnish, swedish, english, short, latin) {
        return {finnish, swedish, english, short, latin};
      }
      
      const rows = [
        createData(),
      ];
      
    return(
        <div>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead sx={{backgroundColor: 'lightgray'}}>
                    <TableRow>
                        <TableCell><Typography fontWeight="Bold">Finnish</Typography></TableCell>
                        <TableCell align="left"><Typography fontWeight="Bold">Swedish</Typography></TableCell>
                        <TableCell align="left"><Typography fontWeight="Bold">English</Typography></TableCell>
                        <TableCell align="left"><Typography fontWeight="Bold">Short</Typography></TableCell>
                        <TableCell align="left"><Typography fontWeight="Bold">Latin</Typography></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {birds.map((row) => (
                        <TableRow
                        key={row.finnish}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                        <TableCell component="th" scope="row">
                            {row.finnish}
                        </TableCell>
                        <TableCell align="right">{row.swedish}</TableCell>
                        <TableCell align="right">{row.english}</TableCell>
                        <TableCell align="right">{row.short}</TableCell>
                        <TableCell align="right">{row.latin}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                </TableContainer>
        </div>
    )
}

export default TableExample;